import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Repuesto } from '../models/repuesto'

@Injectable()
export class RepuestosSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	private headers = new Headers({
		'Content-type': 'application/json',
	});

	getRepuestos(){
		return this._http.get(this.url+'repuestos/all').map(res=> res.json());
	}

	getRepuesto(id){
		return this._http.get(this.url+'repuestos/'+ id).map(res=> res.json());
	}

	getRepuestosByCategoria(id){
		return this._http.get(this.url+'repuestos/categoria/'+id).map(res=> res.json());
	}
}