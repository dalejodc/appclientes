/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; //para poder hacer binding de inputs
import { HttpModule } from '@angular/http'; //Para usar peticiones
import { MaterializeModule } from 'angular2-materialize';
import { AppComponent } from './app.component';
import { routing, appRoutingProviders} from "./app.routing"; //Para las rutas

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { BarraNavegacionComponent } from './components/barraNavegacion.component';
import { LoginComponent } from './components/login.component';
import { RepuestosComponent } from './components/repuestos.component';
import { FooterComponent } from './components/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    BarraNavegacionComponent,
    LoginComponent,
    RepuestosComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    routing,
    FormsModule,
    HttpModule
  ],
  providers: [ appRoutingProviders],
  bootstrap: [ AppComponent]
})
export class AppModule { }
