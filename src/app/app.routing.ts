/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule} from "@angular/router";

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { LoginComponent } from './components/login.component';
import { RepuestosComponent } from './components/repuestos.component';

/*=============================================================================================
              ARREGLO DE LAS RUTAS
===============================================================================================*/
const appRoutes: Routes = [
	{path: '', component: HomeComponent}, //Página Home, ruta inicial

	{path: 'home', component: HomeComponent},
	{path: 'login', component: LoginComponent},
	{path: 'repuestos', component: RepuestosComponent},
	{path: 'repuestos/:id', component: RepuestosComponent},

	{path: '**', component: ErrorComponent} //Página para errores, cuando la ruta falla
]


//Procedimiento que necesita Angular
export const appRoutingProviders: any[] = [];
/*
-Variable routing de tipo ModuleWithProviders
-Con el método .forRoot le decimos qué arreglo tiene que cargar para las rutas, tiene que ser el
que habíamos creado antes. 
-Agara todas las rutas que le hemos indicado, las introduzca y las inyecte en la configuración de rutas del 
framework */
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);