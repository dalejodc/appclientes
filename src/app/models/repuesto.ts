// import { Categoria } from './categoria'
// import { Vehiculo } from './vehiculo'
// import { Inventario } from './inventario'
// import { Marca } from './marca'

export class Repuesto{

	constructor(
	public id: number,
	public nombre: string,
	public descripcion: string,
	public precio: number
	)
	{}
}