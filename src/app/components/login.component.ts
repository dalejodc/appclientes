import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
	selector: 'login',
	templateUrl: '../views/login.component.html',
	providers: []
})

export class LoginComponent{
	public user="";
	public password="";
	
	constructor(
		private _router: Router
		){
	}

	ngOnInit(){
	}

	login(){
		if(this.user=="admin" && this.password=="1234"){
			this._router.navigate(['/home']);
			console.log("viene");
			// window.location.href='http://localhost:4201/home';
		}else{
			alert("Credenciales incorrectas");
			this.user="";
			this.password="";
		}
	}

}