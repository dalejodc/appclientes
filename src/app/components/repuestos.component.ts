import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { Repuesto } from '../models/repuesto'
import { RepuestosSV } from '../services/repuestos.service'

@Component({
	selector: 'repuestos',
	templateUrl: '../views/repuestos.component.html',
	providers: [RepuestosSV]
})

export class RepuestosComponent{
	public id: number;
	public title: string;
	public listaRepuestos : Repuesto[];
	public listaRepuestosCategoria : Repuesto[];

	constructor(
		private _repuestoSV:RepuestosSV,
		private _route: ActivatedRoute,
	){
		this.title = "RepuestosComponents Funciona";
	}

	ngOnInit(){

		this._route.params.forEach((params: Params)=>{
			this.id = params['id'];

			if(this.id != null && params['id']){
			this._repuestoSV.getRepuestosByCategoria(this.id).subscribe(
				response =>{
					this.listaRepuestos = response;
					console.log("Categoria traida:", response);
				},
				error =>{
					console.log(<any>error);
				}
			);
			}else{
			this.getRepuestos();
		}
		});
	}

	getRepuestos(){
		this._repuestoSV.getRepuestos().subscribe(
			result => {
				console.log(result);
				this.listaRepuestos = result;
			},
			error => {
				console.log(<any>error);
			}
		);
	}
}