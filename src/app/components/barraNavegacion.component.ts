import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { RepuestosSV } from '../services/repuestos.service';
import { Repuesto } from '../models/repuesto'

import { CategoriasSV } from '../services/categorias.service';
import { Categoria } from '../models/categoria'

@Component({
	selector: 'barraNavegacion',
	templateUrl: '../views/barraNavegacion.component.html',
	providers: [CategoriasSV, RepuestosSV]
})

export class BarraNavegacionComponent{
	public listaCategorias: Categoria[];
	public listaRepuestos: Repuesto[];
	
	constructor(
		private _categoriaSV : CategoriasSV,
		private _repuestosSV : RepuestosSV,
		private _router: Router,
	){
		
	}

	ngOnInit(){
		this.getCategorias();
	}

	getCategorias(){
		this._categoriaSV.getCategorias().subscribe(
			result => {
				console.log(result);
				this.listaCategorias = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getRepuestosCategoria(id){
		this._router.navigate(['/repuestos/'+id]);
		// this._router.navigate(['/repuestos']);
		this._repuestosSV.getRepuestosByCategoria(id).subscribe(
			result => {
				console.log(result);
				this.listaRepuestos = result;
			},
			error => {
				console.log(<any>error);
			}
		);
	}
}